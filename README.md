# Summary
### This project is used for JPMorgan & Chase's Android coding challenge.

# Comments
### - This application is currently only able to list all of the schools and is unable to display SAT scores depending on the school chosen.

### - I've managed to figure that the two endpoints were connected through "dbn" and that all I had to do was navigate from the school list endpoint to the SAT scores endpoint. I wanted to implement the Navigation Component to be able to navigate between them but unfortunately was out of time.

### - Although unfinished, this application has taught me a lot about planning and helped refreshed my memory on some of the core concepts!