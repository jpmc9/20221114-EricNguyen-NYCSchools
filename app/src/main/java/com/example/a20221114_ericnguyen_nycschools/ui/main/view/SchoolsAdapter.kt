package com.example.a20221114_ericnguyen_nycschools.ui.main.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.a20221114_ericnguyen_nycschools.R
import com.example.a20221114_ericnguyen_nycschools.domain.models.SchoolItemDomain

class SchoolsAdapter : RecyclerView.Adapter<SchoolsAdapter.SchoolViewHolder>() {
    inner class SchoolViewHolder(schoolView: View) : RecyclerView.ViewHolder(schoolView) {
        var schoolName: TextView
        init { schoolName = schoolView.findViewById(R.id.tvSchoolName) }
    }

    private var schoolsList: MutableList<SchoolItemDomain> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_school, parent, false)

        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.schoolName.text = schoolsList[position].schoolName
    }

    override fun getItemCount(): Int = schoolsList.size

    fun updateSchoolsList(schools: List<SchoolItemDomain>) {
        val oldSize = schools.size
        schoolsList.addAll(schools)
        notifyItemRangeChanged(oldSize, schoolsList.size)
    }
}