package com.example.a20221114_ericnguyen_nycschools.ui.main.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20221114_ericnguyen_nycschools.databinding.FragmentMainBinding
import com.example.a20221114_ericnguyen_nycschools.ui.main.viewmodel.MainViewModel
import com.example.a20221114_ericnguyen_nycschools.ui.util.collectLatestLifecycleFlow

class MainFragment : Fragment() {
    private lateinit var fragmentMainBinding: FragmentMainBinding

    companion object { fun newInstance() = MainFragment() }

    private val schoolAdapter by lazy { SchoolsAdapter() }
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)
        fragmentMainBinding.rvSchoolList.adapter = schoolAdapter
        fragmentMainBinding.rvSchoolList.layoutManager = LinearLayoutManager(requireContext())

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.getSchools()

        initObservers()

        return fragmentMainBinding.root
    }

    private fun initObservers() {
        val TAG = "initObservers"

        collectLatestLifecycleFlow(viewModel.state) { state ->
            Log.e(TAG, "isLoading: ${state.isLoading}, schools count: ${state.schools.size}")
            if(state.schools.isNotEmpty()) { schoolAdapter.updateSchoolsList(state.schools) }
        }
    }
}