package com.example.a20221114_ericnguyen_nycschools.ui.main.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20221114_ericnguyen_nycschools.domain.usecases.GetSchoolsUseCase
import com.example.a20221114_ericnguyen_nycschools.ui.main.view.MainState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val TAG = "MainViewModel"
    private val getSchoolsUseCase = GetSchoolsUseCase()

//    Hold state here to save configuration changes ie. device orientation
    private val _state = MutableStateFlow(MainState())
    val state = _state.asStateFlow()

    fun getSchools() {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true)

            Log.e(TAG, "isLoading: ${_state.value.isLoading}")

            getSchoolsUseCase().collectLatest { schools ->
                _state.value = MainState(schools = schools)
            }
        }
    }
}