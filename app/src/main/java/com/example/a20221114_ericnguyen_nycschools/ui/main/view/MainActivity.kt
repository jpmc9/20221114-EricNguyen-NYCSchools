package com.example.a20221114_ericnguyen_nycschools.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a20221114_ericnguyen_nycschools.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.activity_main, MainFragment.newInstance())
                    .commitNow()
        }
    }
}