package com.example.a20221114_ericnguyen_nycschools.ui.main.view

import com.example.a20221114_ericnguyen_nycschools.domain.models.SchoolItemDomain

data class MainState(
    val isLoading: Boolean = false,
    val schools: List<SchoolItemDomain> = emptyList()
)