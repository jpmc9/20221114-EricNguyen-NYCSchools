package com.example.a20221114_ericnguyen_nycschools.data.remote

import com.example.a20221114_ericnguyen_nycschools.data.remote.models.SatScores
import com.example.a20221114_ericnguyen_nycschools.data.remote.models.Schools
import retrofit2.http.GET

// Interface to define REST API operations (GET, POST, PUT, DELETE)
interface SchoolService {
    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
        private const val SCHOOL_ENDPOINT = "s3k6-pzi2.json"
        private const val SAT_ENDPOINT = "f9bf-2cp4.json"
    }

    @GET(SCHOOL_ENDPOINT)
    suspend fun getSchools(): Schools

    @GET(SAT_ENDPOINT)
    suspend fun getSatScores(): SatScores
}