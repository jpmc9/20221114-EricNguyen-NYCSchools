import com.example.a20221114_ericnguyen_nycschools.data.remote.SchoolService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

/**
 * Separate the Retrofit object from the Retrofit service to adhere to the Single Responsibility
 * Principle, creating modular code and makes it easier to make changes and understand.
 *
 * Guarantees that the Retrofit object will be a singleton, preventing unnecessary memory allocation.
 */
object RetrofitProvider {
    private val retrofitObject = Retrofit
        .Builder()
        .addConverterFactory(GsonConverterFactory.create())

    fun getSchoolService(): SchoolService {
        return retrofitObject
            .baseUrl(SchoolService.BASE_URL)
            .build()
            .create()
    }
}