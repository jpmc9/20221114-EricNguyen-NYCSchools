package com.example.a20221114_ericnguyen_nycschools.data

import RetrofitProvider
import android.util.Log
import com.example.a20221114_ericnguyen_nycschools.data.remote.models.SatScores
import com.example.a20221114_ericnguyen_nycschools.data.remote.models.Schools
import com.example.a20221114_ericnguyen_nycschools.domain.repositories.SchoolRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

// Implementation for the SchoolRepository interface created in the domain layer
object SchoolRepoImpl: SchoolRepository {
    private const val TAG = "SchoolRepoImpl"
    private val schoolService = RetrofitProvider.getSchoolService()

    override suspend fun getSchools(): Flow<Schools> = callbackFlow {
        try {
            val result = schoolService.getSchools()
            send(result)
        } catch (error: java.lang.Exception) {
            Log.e(TAG, "There was an error getting all schools!: ${error.message}")
        }
        awaitClose { this.cancel() }
    }

    override suspend fun getSatScores(): Flow<SatScores> {
        TODO("Not yet implemented")
    }
}