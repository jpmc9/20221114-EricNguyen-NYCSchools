package com.example.a20221114_ericnguyen_nycschools.domain.models

data class SchoolsDomain(
    val schools: List<SchoolsDomain> = emptyList()
)