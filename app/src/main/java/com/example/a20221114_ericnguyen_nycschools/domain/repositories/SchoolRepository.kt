package com.example.a20221114_ericnguyen_nycschools.domain.repositories

import com.example.a20221114_ericnguyen_nycschools.data.remote.models.SatScores
import com.example.a20221114_ericnguyen_nycschools.data.remote.models.Schools
import kotlinx.coroutines.flow.Flow

/**
 * I wanted to be able to make it easier to change
 * repository method implementations within a usecase
 */
interface SchoolRepository {
    suspend fun getSchools(): Flow<Schools>
    suspend fun getSatScores(): Flow<SatScores>
}