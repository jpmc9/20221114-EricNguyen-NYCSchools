package com.example.a20221114_ericnguyen_nycschools.domain.usecases

import com.example.a20221114_ericnguyen_nycschools.data.SchoolRepoImpl
import com.example.a20221114_ericnguyen_nycschools.domain.models.SchoolItemDomain
import com.example.a20221114_ericnguyen_nycschools.domain.models.mappers.mapToDomainList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * Because the data layer's responsibility is for storing and retrieving data, I wanted to
 * implement usecases to separate the business logic from it (switching dispatchers, mapping data objects
 * to domain objects, etc.)
 */
class GetSchoolsUseCase {
    private val coroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
    private val schoolRepository = SchoolRepoImpl

    suspend operator fun invoke(): Flow<List<SchoolItemDomain>> = callbackFlow {
        val job = coroutineScope.launch {
            schoolRepository.getSchools().collectLatest { schools ->
                send(schools.mapToDomainList())
            }
        }

        awaitClose { job.cancel() }
    }
}