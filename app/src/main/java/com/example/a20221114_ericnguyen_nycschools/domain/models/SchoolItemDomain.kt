package com.example.a20221114_ericnguyen_nycschools.domain.models

/**
 * I wanted to separate the data object to a domain object
 * to control which properties from the data object I wanted to use.
 */
data class SchoolItemDomain(
    val schoolName: String,
    val overview: String,
    val location: String,
    val phoneNumber: String,
    val faxNumber: String?,
    val schoolEmail: String?,
    val schoolWebsite: String?,
    val totalStudents: Int
)