package com.example.a20221114_ericnguyen_nycschools.domain.models.mappers

import com.example.a20221114_ericnguyen_nycschools.data.remote.models.SchoolItem
import com.example.a20221114_ericnguyen_nycschools.data.remote.models.Schools
import com.example.a20221114_ericnguyen_nycschools.domain.models.SchoolItemDomain

// Map SchoolItem data to SchoolItemDomain object for use in the presentation layer of the application
fun SchoolItem.mapToDomain(): SchoolItemDomain {
    return SchoolItemDomain(
        schoolName = this.school_name,
        schoolEmail = this.school_email,
        schoolWebsite = this.website,
        location = this.location,
        overview = this.overview_paragraph,
        faxNumber = this.fax_number,
        totalStudents = this.total_students.toInt(),
        phoneNumber = this.phone_number
    )
}

fun Schools.mapToDomainList(): List<SchoolItemDomain> = this.map {
    schoolItem -> schoolItem.mapToDomain()
}